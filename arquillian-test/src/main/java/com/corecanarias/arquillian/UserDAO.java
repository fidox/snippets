package com.corecanarias.arquillian;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.corecanarias.arquillian.model.UserEntity;

@Stateless
public class UserDAO {
	@PersistenceContext private EntityManager entityManager;
	
	public void persist(UserEntity entity) {
		entityManager.persist(entity);				
	}
	
	public UserEntity findByUsername(String username) {
		Query q = entityManager.createQuery("from USERS where username = :username");
		q.setParameter("username", username);
		return (UserEntity)q.getSingleResult();		
	}
}