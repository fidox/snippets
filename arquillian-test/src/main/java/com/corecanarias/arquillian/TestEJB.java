package com.corecanarias.arquillian;

import javax.ejb.Stateless;

@Stateless
public class TestEJB {

	public String test(String var) {
		return var + " Hello World!";
	}
}
