package com.corecanarias.arquillian;

import javax.ejb.EJB;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.corecanarias.arquillian.model.UserEntity;

@RunWith(Arquillian.class)
public class TestArquillianEJB {

	@Deployment
	public static JavaArchive createDeployment() {
		JavaArchive archive = ShrinkWrap.create(JavaArchive.class, "tests.jar").addPackages(true, "com.corecanarias").
				addAsManifestResource("persistence.xml", "persistence.xml")
				;
		System.out.println(archive.toString(true));
		return archive;
	}

	@EJB(mappedName="java:module/TestEJB!com.corecanarias.arquillian.TestEJB") private TestEJB subject;	
	@EJB(mappedName="java:module/UserDAO!com.corecanarias.arquillian.UserDAO") private UserDAO jpa;
	
	@Test
	public void testEJB() {
	
		System.out.println(subject.test("test..."));
	}
	
	@Test
	public void testJPA() {
		UserEntity user = new UserEntity();
		user.setUsername("username");
		user.setPassword("password");
		jpa.persist(user);
		
		user = jpa.findByUsername("username");
		System.out.println(user.getId());
		System.out.println(user.getUsername());
		System.out.println(user.getPassword());
	}
}