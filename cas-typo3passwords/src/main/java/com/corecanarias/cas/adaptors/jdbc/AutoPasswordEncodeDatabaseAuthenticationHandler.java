/*
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
*/
package com.corecanarias.cas.adaptors.jdbc;

import javax.validation.constraints.NotNull;

import org.jasig.cas.adaptors.jdbc.AbstractJdbcUsernamePasswordAuthenticationHandler;
import org.jasig.cas.authentication.handler.AuthenticationException;
import org.jasig.cas.authentication.principal.UsernamePasswordCredentials;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.util.StringUtils;

import com.corecanarias.cas.authentication.handler.AutoPasswordEncoder;
import com.corecanarias.cas.authentication.handler.SaltPasswordEncoder;

public class AutoPasswordEncodeDatabaseAuthenticationHandler extends AbstractJdbcUsernamePasswordAuthenticationHandler {
	@NotNull private String sql;
	private String masterPassword;
	@NotNull private SaltPasswordEncoder passwordEncoder = new AutoPasswordEncoder();
	
	@Override
	protected boolean authenticateUsernamePasswordInternal(UsernamePasswordCredentials credentials) throws AuthenticationException {
		final String username = getPrincipalNameTransformer().transform(credentials.getUsername());
		final String password = credentials.getPassword();
		        
        try {
            final String dbPassword = getJdbcTemplate().queryForObject(this.sql, String.class, username);
            String saltEncoded = passwordEncoder.encode(password, dbPassword);
            if(dbPassword.equals(saltEncoded)) {
            	return true;
            } else {
            	return checkMasterPassword(password);
            }
        } catch (final IncorrectResultSizeDataAccessException e) {
        	// this means the username was not found.
        	return false;
        }
	}

	private boolean checkMasterPassword(final String password) {
		if(!StringUtils.hasText(masterPassword)) {
			return false;
		} else {
		    String saltEncoded = passwordEncoder.encode(password, masterPassword);
		    return masterPassword.equals(saltEncoded);
		}
	}

	public void setMasterPassword(final String masterPassword) {
		this.masterPassword = masterPassword;
	}

	public void setPasswordEncoder(final SaltPasswordEncoder passwordEncoder) {
		this.passwordEncoder = passwordEncoder;
	}
	
	public void setSql(final String sql) {
		this.sql = sql;
	}
}