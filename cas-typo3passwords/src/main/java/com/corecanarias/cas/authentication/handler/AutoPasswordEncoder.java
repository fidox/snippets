/*
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
*/
package com.corecanarias.cas.authentication.handler;

import java.security.NoSuchAlgorithmException;

import javax.validation.constraints.NotNull;

import org.jasig.cas.authentication.handler.DefaultPasswordEncoder;
import org.springframework.util.StringUtils;

import com.corecanarias.cas.utils.MD5Crypt;

public class AutoPasswordEncoder implements SaltPasswordEncoder {
	@NotNull private String encodingAlgorithm = "UTF-8";

	public String encode(final String password) {
		if(!StringUtils.hasText(encodingAlgorithm)) {
			throw new RuntimeException("Encodign algorithm is not set in AutoPasswordEncoder");
		}
		final DefaultPasswordEncoder encoder = new DefaultPasswordEncoder(encodingAlgorithm);
		return encoder.encode(password);
	}

	public String encode(final String password, final String salt) {
		if(salt.startsWith("$1$")) {
			try {
				return MD5Crypt.crypt(password, salt);
			} catch (NoSuchAlgorithmException e) {
				throw new RuntimeException(e);
			}			
		} else {
			return password;
		}
	}
	
	public void setEncodingAlgorithm(final String encodingAlgorithm) {
		this.encodingAlgorithm = encodingAlgorithm;
	}
}